package main


// Block represents each block in the blockchain
type Block struct{
	Index     int `json:"index"`
	Timestamp string `json:"timestamp"`
	Data       int `json:"data"`
	Hash      string `json:"hash"`
	PrevHash  string `json:"prevHash"`

}

// Blockchain as a series of block
var Blockchain []Block

// Message takes incoming JSON payload for writing heart rate
type Message struct {
	Data int `json:"data"`
}